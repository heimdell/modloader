  The plan is as follows:

  - This thing manages modpack, by putting just the right mods for the current
    (or selected) modpack.

  - It only downloads mods if they are new ones.

  - It manages mod cache, so the mods can be updated themselves.

  - It can update (and revert) configs as well.

  - It can update itself to the new version.

  - It is self-contained, so to install it you just copy the folder somewhere.

  - Probably, we need the installer program that does it.