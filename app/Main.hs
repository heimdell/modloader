
import Main.Utf8

import System.Environment

import Mod

main = withUtf8 do
  getArgs >>= (readAllModsFrom . head) >>= print