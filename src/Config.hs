
module Config where

data Config = Config
  { root     :: FilePath
  , dumpster :: FilePath
  }