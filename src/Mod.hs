
module Mod where

import Codec.Archive.Zip (mkEntrySelector, getEntrySource, withArchive)

import Control.Applicative (some)
import Control.Monad.Trans.Resource (ResourceT, runResourceT)
import Control.Monad.Catch (catchAll)
import Control.Monad.IO.Class (liftIO)
import Control.Monad (void)

import qualified Data.Aeson as Aeson
import Data.Sequences (fromChunks)
import Data.Conduit (await, runConduit, (.|))
import Data.Function (on)
import Data.Traversable (for)
import Data.List (intercalate)
import qualified Data.Map as Map
import Data.Map (Map)
import qualified Data.Set as Set
import Data.Set (Set)
import Data.Maybe (fromMaybe, catMaybes)

import GHC.Generics (Generic)

import Text.ParserCombinators.Parsec

import System.Directory (listDirectory)
import System.FilePath ((</>), splitExtension)

import Debug.Trace

-- | Collection of mods, existing on a machine.
--
--   Is locked to a specific game versions.
--   (Since Mojang can't produce a mod API in this century anyway)
--
data Library = Library
  { mods :: Map Name (Map ModVersion Mod)
  }
  deriving stock (Eq, Generic)
  deriving anyclass (Aeson.FromJSON, Aeson.ToJSON)

-- | Library.toString
--
instance Show Library where
  show = unlines . map show . toList

-- | The mod itself
--
data Mod = Mod
  { modId   :: ModId
  , deps    :: [Dependency]
  , enabled :: Bool         -- ^ If the mod is in use, or disabled
  }
  deriving stock (Eq, Generic)
  deriving anyclass (Aeson.FromJSON, Aeson.ToJSON)

-- | Compare mod by comparing IDs.
--
instance Ord Mod where
  compare = compare `on` modId

-- | Mod.toString
--
instance Show Mod where
  show (Mod mid deps enabled) =
    (if enabled then "[✓] " else "[✗] ")
    <> show mid
    <> case deps of
      [] -> ""
      _  -> ":" ++ (("\n    -   " ++) . show =<< deps)

-- | A `ModId`, but with diapasone for version.
--
data Dependency = Dependency
  { depName   :: Name
  , diapasone :: (Maybe ModVersion, Maybe ModVersion)
  }
  deriving stock (Eq, Ord, Generic)
  deriving anyclass (Aeson.FromJSON, Aeson.ToJSON)

-- | Dependency.toString
--
instance Show Dependency where
  show (Dependency name (mi, ma)) =
    show name <> case (mi, ma) of
      (Nothing, Nothing) -> ""
      (Nothing, Just ma) -> "@[," <> show ma <> "]"
      (Just mi, Nothing) -> "@[" <> show mi <> ",]"
      (Just mi, Just ma) -> "@[" <> show mi <> "," <> show ma <> "]"

-- | A mod identifier.
--
data ModId = ModId
  { name        :: Name
  , version     :: ModVersion
  }
  deriving stock (Eq, Ord, Generic)
  deriving anyclass (Aeson.FromJSON, Aeson.ToJSON)

-- | ModId.toString
--
instance Show ModId where
  show (ModId name v) = show name <> "-" <> show v

-- | This is the sequence of numbers found in the version string.
--
data ModVersion = ModVersion
  { numbers :: [Int]
  }
  deriving stock (Eq, Ord, Generic)
  deriving anyclass
    ( Aeson.FromJSON
    , Aeson.ToJSON
    , Aeson.FromJSONKey
    , Aeson.ToJSONKey
    )

-- | ModVersion.toString
--
instance Show ModVersion where
  show = intercalate "." . map show . numbers

newtype Name = Name { unName :: String }
  deriving newtype (Eq, Ord, Generic)
  deriving newtype
    ( Aeson.FromJSON
    , Aeson.ToJSON
    , Aeson.FromJSONKey
    , Aeson.ToJSONKey
    )

-- | Name.toString
--
instance Show Name where
  show = unName

-- | Ability to merge 2 libraries together.
--
instance Semigroup Library where
  Library l1 <> Library l2
    = Library
    $ Map.unionWith Map.union l1 l2

-- | Ability to make an empty libary.
--
instance Monoid Library where
  mempty = Library Map.empty

-- | Make a library out of one mod.
--
singleton :: Mod -> Library
singleton mod
  = Library
  $ Map.singleton (name (modId mod))
  $ Map.singleton (version (modId mod))
  $ mod

-- | Add mod to the `Library`.
--
add :: Mod -> Library -> Library
add mod = (singleton mod <>)

-- | Turn all mods into the libraries, then merge.
--
fromList :: [Mod] -> Library
fromList = foldMap singleton

-- | Extract list of mods out of the library.
--
toList :: Library -> [Mod]
toList = (foldMap.foldMap) pure . mods

-- | Find a mod.
--
find :: ModId -> Library -> Maybe Mod
find mod (Library mods) =
  Map.lookup (name mod) mods >>=
  Map.lookup (version (mod :: ModId))

-- | Find all solutions to the dependency.
--
--   Find a name, and then peforms 0-2 splits over version map.
--
solutions :: Dependency -> Library -> [Mod]
solutions (Dependency name (mminV, mmaxV)) (Library mods) =
    fromMaybe [] $ fmap split $ Map.lookup name $ mods
  where
    split byVersion = Map.elems result
      where
        after  = maybe byVersion (snd . (`split'` byVersion)) mminV
        result = maybe after     (fst . (`split'` after))     mmaxV

-- Split map in two, so in first one all keys < k, and in the second one >= k.
--
split' :: Ord k => k -> Map k v -> (Map k v, Map k v)
split' k m =
  case Map.splitLookup k m of
    (l, Just v, r) -> (l, Map.singleton k v <> r)
    (l, _,      r) -> (l, r)

-- | A lazy tree of all possible solutions to the dependency.
--
data Resolution = Resolution
  { node :: Either Dependency [(Mod, ResolutionForest)]
  }
  deriving stock (Show)

-- | A lazy forest of all possible solutions to the dependency.
--
type ResolutionForest = [Resolution]

-- | Make lazy forest representing all possible solutions to the set of mods.
--
resolve :: Library -> [ModId] -> ResolutionForest
resolve lib = resolveMods
  where
    resolveMods = foldMap resolveMod
    resolveMod modId =
      case find modId lib of
        Nothing -> return
          $ Resolution
          $ Left
          $ Dependency (name modId)
            ( Just (version (modId :: ModId))
            , Just (version (modId :: ModId))
            )

        Just mod -> return
          $ Resolution
          $ Right [(mod, resolveDep =<< deps mod)]

    resolveDep dep =
      case solutions dep lib of
        [] -> return
          $ Resolution
          $ Left dep

        mods -> resolveMods $ map modId mods

-- | Take the solution with lowest version, even if it contains errors.
--
firstSolution :: ResolutionForest -> Set (Either Dependency ModId)
firstSolution = foldMap \(Resolution node) ->
  case node of
    Left dep ->
      Set.singleton (Left dep)

    Right ((mod, deps) : _) ->
      Set.singleton (Right (modId mod)) <> firstSolution deps

-- | Set of constraints on mods.
--
--   Is a monoid with empty set as zero element and join as semigroup addition.
--
newtype Constraints = Constraints
  { unDepLattice :: Map Name (Maybe ModVersion, Maybe ModVersion)
  }

-- | Find intersection of 2 constraint sets.
--
instance Semigroup Constraints where
  Constraints l <> Constraints r = loop (Map.toList l) r
    where
      loop [] r = Constraints r
      loop ((name, (mi, ma)) : l) r =
        case Map.splitLookup name r of
          (_, Nothing, _) ->
            Constraints
            $  Map.singleton name (mi, ma)
            <> unDepLattice (loop l r)

          (before, Just (mi', ma'), after) ->
            Constraints
            $ Map.singleton name (max' mi mi', min' ma ma')
            <> unDepLattice (loop l (before <> after))
        where
          max' (Just x) (Just y) = Just $ max x y
          max' (Just x)  _       = Just $ x
          max'  _       (Just y) = Just $ y
          max'  _        _       = Nothing

          min' (Just x) (Just y) = Just $ min x y
          min' (Just x)  _       = Just $ x
          min'  _       (Just y) = Just $ y
          min'  _        _       = Nothing

-- | Empty constraint set.
--
instance Monoid Constraints where
  mempty = Constraints mempty

-- | Take out unresolved deps and make them name-unique, intersecting
--   version diapasones.
--
collideDependencies
  :: Set (Either Dependency ModId)
  -> Set (Either Dependency ModId)
collideDependencies set =
    mods <> deps'
  where
    (deps, mods) = Set.partition (either (const True) (const False)) set

    deps'
      = Set.map Left
      $ untie
      $ foldMap asLattice
        [dep | Left dep <- Set.toList deps]

    asLattice :: Dependency -> Constraints
    asLattice (Dependency name (mi, ma)) =
      Constraints
      $ Map.singleton name (mi, ma)

    untie = Set.fromList . map (uncurry Dependency) . Map.toList . unDepLattice

-- | Example library.
--
example :: Library
example = fromList
  [ Mod (mkModId "abc-1.2.3") [ mkDep "foo@[1.2.3,]"      ] True
  , Mod (mkModId "cde-1.2.3") [ mkDep "foo@[1.2.3,3.2.1]" ] True
  , Mod (mkModId "def-1.2.3") [ mkDep "foo@[1.2.5,3.4.5]" ] True
  ]

-- | Test "get first solution" mechanism.
--
test :: Set (Either Dependency ModId)
test
  = collideDependencies
  $ firstSolution
  $ resolve example
    [ ModId (Name "abc") (ModVersion [1,2,3])
    , ModId (Name "cde") (ModVersion [1,2,3])
    , ModId (Name "def") (ModVersion [1,2,3])
    ]

-- | A reification of mcmod.info inner structure.
data McModInfo = McModInfo
  { mm_modid        :: String
  , mm_version      :: String
  , mm_requiredMods :: Maybe [String]
  }
  deriving stock (Eq, Ord, Show, Generic)

-- | A smart `ModId` constructor.
--
--   Throws `error` unless string is valid.
--
mkModId :: String -> ModId
mkModId = either (error . show) id . parse modIdP "-"

-- | A smart `Dependency` constructor.
--
--   Throws `error` unless string is valid.
--
mkDep :: String -> Dependency
mkDep = either (error . show) id . parse depP "-"

-- | `ModId` `Parser.
modIdP :: Parser ModId
modIdP = do
  name <- nameP
  version <- versionP
  return ModId {name, version}

-- | Parser for varios "padding" between version numbers,
--   like ".", "-", "_", "_for_", etc.
--
--   Stops at commas and closing parentheses/brackets.
--
notDigits :: Parser ()
notDigits = void $ many $ noneOf $ ['0'.. '9'] <> "]),"

-- | Convert mcmod.info to actual Mod data.
--
--   If some fields can't be understood, whole result will be `Nothing`.
--
--   The `mm_requiredMods` field will be concatenated over ","
--   /and only then/ parsed.
--
parseMod :: McModInfo -> Maybe Mod
parseMod info = do
  let reqs  = maybe "" (intercalate ",") (mm_requiredMods info)
  let vers  = mm_version info
  let vers' = mm_modid info <> ": " <> vers
  version <- just $ parse versionP vers' vers
  deps    <- just $ parse depsP    reqs  reqs
  return Mod
    { modId = ModId
      { name = Name $ mm_modid info
      , version
      }
    , deps
    , enabled = True
    }

-- | Parser for `ModVersion`.
--
versionP :: Parser ModVersion
versionP = do
  notDigits
  nums <- some $ some digit <* notDigits
  return $ ModVersion $ map read nums

-- | Parser for `Dependency` list, comma-separated.
--
depsP :: Parser [Dependency]
depsP = (depP `sepBy` string ",") <* eof

-- | Parser for `Dependency`.
--
--   Should we give a fuck about open/closed intervals, like [3, 4)?
--
--   I don't think so.
--
depP :: Parser Dependency
depP = do
  depName <- nameP
  deps <- optionMaybe do
    string "@"
    string "[" <|> string "("
    mi <- optionMaybe versionP
    string ","
    ma <- optionMaybe versionP
    string "]" <|> string ")"
    return (mi, ma)

  return $ Dependency depName case deps of
    Nothing -> (Nothing, Nothing)
    Just p  -> p

-- | A parser for `Name`.
--
--   A name is (([a-zA-z_][a-zA-Z0-9_]*)-)+.
--
nameP :: Parser Name
nameP = (Name . intercalate "-")
    <$> chunk `sepEndBy1` string "-"
  where
    chunk = (:)
      <$>      (letter   <|> char '_')
      <*> many (alphaNum <|> char '_')

-- | Ignore an error (but write it to the console).
--
just :: Show e => Either e a -> Maybe a
just = either (\e -> traceShow e $ Nothing) Just

-- | Parse McModInfo from JSON.
--
instance Aeson.FromJSON McModInfo where
  parseJSON = Aeson.genericParseJSON Aeson.defaultOptions
    { Aeson.fieldLabelModifier = drop 3
    }

-- | Read all mods from a .jar-file.
--
--   If the mcmod.info is missing or corrupted, `Nothing` is returned.
--
readModId
  :: FilePath
  -> ResourceT IO (Maybe [Mod])
readModId path = do
    source <- withArchive path do
      getEntrySource =<< mkEntrySelector "mcmod.info"

    mbContents <- runConduit (source .| awaitAll)
    return
      $ fmap (catMaybes . map parseMod)
      $ Aeson.decode (fromChunks mbContents)

  `catchAll` \_ -> do
    return Nothing

  where
    awaitAll = do
      ma <- await
      case ma of
        Just a  -> (a :) <$> awaitAll
        Nothing -> return []

-- | Read all mods from all the .jar-files in the given directory.
--
--   Corrupted mods, or non-mod .jars aren't included in the result.
--
readAllModsFrom :: FilePath -> IO Library
readAllModsFrom path = do
  putStrLn "Reading directory listing..."
  list <- listDirectory path
  putStrLn "Checking mods..."
  mmods <- runResourceT do
    for list \file -> do
      if snd (splitExtension file) == ".jar"
      then do
        liftIO $ putStrLn $ "Checking " <> file
        readModId (path </> file)
      else
        return Nothing

  return $ fromList $ concat $ catMaybes mmods