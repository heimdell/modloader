
module Test.Mod (tests) where

import Mod
import Parser
import Printer

import Test.Mod.Instances

import Test.Hspec
import Test.QuickCheck

import Debug.Trace

tests = do
  hspec do
    return ()