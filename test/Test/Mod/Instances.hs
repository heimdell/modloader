
module Test.Mod.Instances () where

import Data.List (intercalate)
import Data.Traversable (for)

import Mod

import Test.QuickCheck

instance Arbitrary Name where
  arbitrary = do
    n      <- elements [1.. 3]
    chunks <- for [1.. n :: Int] \_ -> chunk
    return Name {unName = intercalate "-" chunks}

chunk = do
  h <- initial
  t <- final
  return (h : t)
  where
    initial =          elements $ ['A'.. 'Z'] ++ ['a'.. 'z']
    final   = listOf $ elements $ ['A'.. 'Z'] ++ ['a'.. 'z'] ++ ['0'.. '9']

instance Arbitrary GameVersion where
  arbitrary = elements
    [ GameVersion 1 15 2
    , GameVersion 1 14 4
    , GameVersion 1 12 2
    , GameVersion 1 7 10
    ]

instance Arbitrary ModVersion where
  arbitrary = do
    a <- elements [1.. 3]
    b <- elements [0.. 10]
    c <- elements [0.. 10]
    d <- elements
      [ Just "final"
      , Nothing
      ]
    return $ ModVersion [a, b, c] d

instance Arbitrary ModId where
  arbitrary = ModId
    <$> arbitrary
    <*> arbitrary
    <*> arbitrary

instance Arbitrary Mod where
  arbitrary = Mod
    <$> arbitrary
    <*> arbitrary
    <*> arbitrary

instance Arbitrary Library where
  arbitrary = do
    n <- elements [0.. 10]
    mods <- for [1.. n] \_ -> arbitrary
    return $ foldr addMod emptyLib mods